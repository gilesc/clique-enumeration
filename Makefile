$(shell mkdir -p bin)

lib/libclique.so: 

bin/enumerate-cliques: clique.cpp
	g++ -std=c++2b -o $@ $^

test: clique.py libclique.so
	python clique.py

#g++ -std=c++2b -c -fPIC -O3 -o $@ $^

.PHONY: test
