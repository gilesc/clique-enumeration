import itertools

import networkx as nx

if __name__ == "__main__":
    g = nx.Graph()
    with open("data/caida.pairs") as h:
        for line in h:
            s, t = tuple(map(int, line.strip().split("\t")))
            g.add_edge(s,t)

    i = 0
    for n, cliques in itertools.groupby(nx.enumerate_all_cliques(g), len):
        print(n, len(list(cliques)))
