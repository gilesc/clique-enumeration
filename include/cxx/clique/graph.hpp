#pragma once

#include <unordered_map>
#include <vector>
#include <cstdint>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <array>
#include <set>

#include <omp.h>

#include <clique/util.hpp>

namespace clique {

typedef uint64_t node_t;
typedef std::vector<node_t> nodes_t;
typedef std::unordered_map<node_t, nodes_t> adjacency_t;
typedef std::array<node_t, 3> triangle_t;
typedef std::vector<triangle_t> triangles_t;

template <size_t K>
using cq = std::array<node_t, K>;

template <size_t K>
using cqs = std::vector<cq<K> >;

adjacency_t
read_pairs(std::istream& input) {
	std::string line;
	size_t i;
	adjacency_t adjacency;
	while (getline(input, line)) {
		i++;
		line = strip(line);
		std::vector<std::string> items = split(line, '\t');
		if (items.size() != 2) {
			std::cerr << "Invalid adjacency pair format on line: " << i << std::endl;
			std::cerr << "Line: '" << line << "'\n";
			std::cerr << "Aborting." << std::endl;
			exit(1);
		}
		node_t s = std::stoull(items[0]);
		node_t t = std::stoull(items[1]);

		if (s == t)
			continue;
		if (s < t) {
			adjacency[s].push_back(t);
		} else {
			adjacency[t].push_back(s);
		}
	}
	size_t n_edges = 0;
	for (auto& p : adjacency) {
		nodes_t& v = p.second;
		nodes_t vv = deduplicate(p.second);
		adjacency[p.first] = vv;
		n_edges += vv.size();
	}

	std::set<node_t> all_nodes;
	for (auto& p : adjacency) {
		all_nodes.insert(p.first);
		for (auto& x : p.second) {
			all_nodes.insert(x);
		}
	}
  for (auto x : all_nodes) {
    nodes_t empty;
    adjacency.try_emplace(x, empty);
  }
	
	std::cerr << "Initialized graph with " 
		<< adjacency.size() 
		<< " nodes and " 
		<< n_edges 
		<< " edges\n";
	return adjacency;
}

template <size_t K>
cqs<K> find_cliques(const adjacency_t& adjacency) {
  cqs<K-1> candidates = find_cliques<K-1>(adjacency);
  cqs<K> o;

  int ncpu = omp_get_max_threads();
  std::vector<cqs<K> > oo(ncpu);

  #pragma omp parallel for shared(oo)
  for (const cq<K-1>& c : candidates) {
    int thread_num = omp_get_thread_num();
    std::vector<nodes_t> nbrs;
    for (node_t n : c) {
      nbrs.push_back(adjacency.at(n));
    }
    std::vector<node_t> ok = intersection(nbrs);
    std::sort(ok.begin(), ok.end());
    for (node_t x : ok) {
      cq<K> rs;
      for (size_t i=0; i<(K-1); i++) {
        rs[i] = c[i];
      }
      rs[K-1] = x;
      oo.at(thread_num).push_back(rs);
    }
  }

  for (auto& v : oo) {
    std::copy(v.begin(), v.end(), std::back_inserter(o));
  }
  std::sort(o.begin(), o.end());

  for (size_t i=0; i<o.size(); i++) {
    std::cout << o[i][0];
    for (size_t j=1; j<o[0].size(); j++) {
      std::cout << "\t" << o[i][j];
    }
    std::cout << std::endl;
  }
  return o;
}

template <>
cqs<3> find_cliques<3>(const adjacency_t& adjacency) {
  int ncpu = omp_get_max_threads();
  std::vector<std::vector<triangle_t> > oo(ncpu);

  std::vector<node_t> v_nodes;
  for (auto p : adjacency)
    v_nodes.push_back(p.first);
  std::sort(v_nodes.begin(), v_nodes.end());

	#pragma omp parallel for shared(v_nodes, oo)
  for (size_t i=0; i<v_nodes.size(); i++) {
    int thread_num = omp_get_thread_num();
    const node_t A = v_nodes[i];
    const nodes_t& Av = adjacency.at(A);
    for (const node_t& B : Av) {
      for (const node_t& C : adjacency.at(B)) {
        if (contains(Av, C)) {
          triangle_t tri;
          tri[0] = A;
          tri[1] = B;
          tri[2] = C;
          oo.at(thread_num).push_back(tri);
        }
      }
    }
  }
  cqs<3> o;
  for (const triangles_t& v : oo) {
    std::copy(v.begin(), v.end(), std::back_inserter(o));
  }
  std::sort(o.begin(), o.end());
  for (size_t i=0; i<o.size(); i++) {
    std::cout << o[i][0];
    for (size_t j=1; j<o[0].size(); j++) {
      std::cout << "\t" << o[i][j];
    }
    std::cout << std::endl;
  }
  return o;
}

class Graph {
private:
  const adjacency_t adjacency;

  nodes_t nodes() {
    nodes_t o;
    for (auto p : adjacency) {
      o.push_back(p.first);
    }
    std::sort(o.begin(), o.end());
    return o;
  }

public:
  Graph(std::istream& input) : adjacency(read_pairs(input)) {}

  void add_edge(node_t s, node_t t);
  cqs<3> triangles();

  template <size_t K>
  cqs<K> cliques() {
    return find_cliques<K>(adjacency);
  }
};



};
