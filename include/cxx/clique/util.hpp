#pragma once

#include <vector>
#include <string>

namespace clique {

std::string
strip (std::string text) {
  size_t start = text.find_first_not_of(" \n\t");
  if (start == std::string::npos) {
    start = 0;
  }
  size_t end = text.find_last_not_of(" \n\t");
  if (end == std::string::npos) {
    end = text.size();
  }
  return text.substr(start, end + 1 - start);
}

std::vector<std::string> 
split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    int start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        tokens.push_back(text.substr(start, end - start));
        start = end + 1;
    }
    tokens.push_back(text.substr(start));
    return tokens;
}

template<class T, class U>
bool contains(const std::vector<T>& container, const U& v)
{
    auto it = std::lower_bound(
        container.begin(),
        container.end(),
        v,
        [](const T& l, const U& r){ return l < r; });
    return it != container.end() && *it == v;
}

template <typename T>
std::vector<T> 
intersection (const std::vector<std::vector<T>> &vecs) {
  auto last_intersection = vecs[0];
  std::vector<T> curr_intersection;

  for (std::size_t i = 1; i < vecs.size(); ++i) {
    std::set_intersection(last_intersection.begin(), last_intersection.end(),
        vecs[i].begin(), vecs[i].end(),
        std::back_inserter(curr_intersection));
    std::swap(last_intersection, curr_intersection);
    curr_intersection.clear();
  }
  return last_intersection;
}

template<class T>
std::vector<T> 
deduplicate(const std::vector<T>& container) {
	std::set<T> elements;
	for (const T& x : container) {
		elements.insert(x);
	} 
	std::vector<T> o(elements.begin(), elements.end());
	return o;
}

};
