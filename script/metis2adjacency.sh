#!/usr/bin/env bash

sed 1d | nl - | while read line; do
    read -ra items <<<"$line"
    n=${#items[@]}

    for (( i=1; i<${n}; i++ ));
    do
        echo -e "${items[0]}\t${items[$i]}"
    done
done | sort -nk1,1 -nk2,2
