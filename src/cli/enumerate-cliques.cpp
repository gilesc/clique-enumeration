#include <clique.hpp>

#include <chrono>

int main(int argc, char* argv[]) {
  clique::Graph g(std::cin);
  int start = clock();

  auto wcts = std::chrono::system_clock::now();
  clique::cqs<20> cliques = g.cliques<20>();
  std::chrono::duration<double> wctduration = (std::chrono::system_clock::now() - wcts);
  std::cerr << "Finished in " << wctduration.count() << " seconds [Wall Clock]" << std::endl;
}
