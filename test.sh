#!/usr/bin/env bash

mkdir -p build
cd build
cmake ..
make -j
cd ..
./build/bin/enumerate-cliques < data/caida.pairs > data/caida.out
